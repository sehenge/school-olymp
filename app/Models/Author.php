<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Author extends Model
{
    use HasFactory;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function books(): BelongsToMany
    {
        return $this->belongsToMany('App\Models\Book', 'author_book', 'authors_id', 'books_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function book(): HasMany
    {
        return $this->hasMany('App\Models\Book', 'books_id', 'books_id');
    }

    /**
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        return "{$this->firstname} {$this->middlename} {$this->lastname}";
    }
}
