<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\View\View;

class BookController extends Controller
{
    /**
     * @return mixed
     */
    public function index(): View
    {
        $books = Book::where('id', '>', 0)->paginate(10);

        return view('books')->withBooks($books);
    }

    /**
     * @param string $search
     * @return mixed
     */
    public function search(string $search): View
    {
        $books = Book::where('name', 'like', '%' . $search . '%')
            ->orWhere('description', 'like', '%' . $search . '%')
            ->paginate(10);

        return view('books')->withBooks($books);
    }
}
