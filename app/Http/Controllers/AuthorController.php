<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\View\View;

class AuthorController extends Controller
{
    /**
     * @return mixed
     */
    public function index(): View
    {
        $authors = Author::where('id', '>', 0)->paginate(10);

        return view('authors')->withAuthors($authors);
    }

    /**
     * @param string $search
     * @return mixed
     */
    public function search(string $search): View
    {
        $authors = Author::where('firstname', 'like', '%' . $search . '%')
            ->orWhere('lastname', 'like', '%' . $search . '%')
            ->orWhere('middlename', 'like', '%' . $search . '%')
            ->paginate(10);

        return view('authors')->withAuthors($authors);
    }
}
