@extends('layouts.app')
@section('content')
    @if ( !$books->count() )
        Нет записей.
    @else
        @php
            $i = 0;
        @endphp
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Название</th>
                <th scope="col">Описание</th>
                <th scope="col">Год издания</th>
                <th scope="col">Авторы</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($books as $book)
                    <tr>
                        <td class="col-md-2">
                            {{ $book->name }}
                        </td>
                        <td class="col-md-6">
                            {{ $book->description }}
                        </td>
                        <td class="col-md-1">
                            {{ $book->release_date }}
                        </td>
                        <td class="col-md-3">
                            @foreach ($book->author as $author)
                                {{ $author->fullname }}
                                <br/>
                                <br/>
                            @endforeach
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $books->links() }}
    @endif
@endsection
