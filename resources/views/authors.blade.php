@extends('layouts.app')
@section('content')
    @if ( !$authors->count() )
        Нет записей.
    @else
        @php
            $i = 0;
        @endphp
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Имя</th>
                <th scope="col">Фамилия</th>
                <th scope="col">Отчество</th>
                <th scope="col">Год Рождения</th>
                <th scope="col">Год смерти</th>
                <th scope="col">Количество книг</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($authors as $author)
                    <tr>
                        <td>
                            {{ $author->firstname }}
                        </td>
                        <td>
                            {{ $author->lastname }}
                        </td>
                        <td>
                            {{ $author->middlename }}
                        </td>
                        <td>
                            {{ $author->dob }}
                        </td>
                        <td>
                            {{ $author->dod }}
                        </td>
                        <td>
                            {{ count($author->books) }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $authors->links() }}
    @endif
@endsection
