function like(id) {
    $.ajax({
        type:'POST',
        url:'/like/' + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
            $.ajax({
                type:'GET',
                url:'/likes/' + id,
                success: function(data){
                    $(".views.id-" + id + " .likes").text(data);
                }
            })
        },
        error: function (data, textStatus, errorThrown) {
            console.log(data);
        }
     });
}

function view(id) {
    $.ajax({
        type:'POST',
        url:'/view/' + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
            $(".views.id-" + id + " .count#views").text(data);
        },
        error: function (data, textStatus, errorThrown) {
            console.log(data);
        }
    });
}


<!-- jQuery Form Validation code -->
$(function() {
    $("#comment").validate({
        rules: {
            title: {
                required: true,
                minlength: 5,
                maxlength: 255
            },
            body: {
                required: true,
                minlength: 1
            },
        },
        // Specify the validation error messages
        messages:
            {
                title: {
                    required: "this field required"
                },
                body:{
                    required:'this field required'
                },
            },
        submitHandler: function()
        {
            let form = $('form')[0];
            let form_action = $("form").attr("action");
            let formData = new FormData(form);
            console.log(formData);
            $.ajax({
                type: 'POST',
                url: form_action,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data)
                {
                    let res = JSON.parse(data);

                    if(res.result)
                    {
                        form.innerHTML = '<div class="alert alert-success" role="alert">' +
                            'Ваше сообщение успешно добавлено' +
                            '</div>';
                    }
                },
                error: function(data)
                {
                    console.log(data);
                }
            });
            return false;
        }
    });
});









$('#post_comment').on('click', function(event){
    console.log('SUBMIT');
    event.preventDefault();

    title = $('#title').val();
    message = $('#body').val();
    post_id = $('#post_id').val();

    $.ajax({
        url: "/comment/add",
        type: "POST",
        data:{
            post_id: post_id,
            title: title,
            message: message,
        },
        success:function(response){
            console.log(response);
            if (response) {
                console.log(response);
            }
        },
        error: function(response) {
            console.log(response);
        }
    });
});
