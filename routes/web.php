<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\AuthorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('books');
});

Route::get('books', [BookController::class, 'index'])->name('books');
Route::get('authors', [AuthorController::class, 'index'])->name('authors');

Route::get('books/{name}', [BookController::class, 'search'])->name('books_search');
Route::get('authors/{name}', [AuthorController::class, 'search'])->name('authors_search');
