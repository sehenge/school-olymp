<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('authors')->insert(array(
            array(
                'firstname' => 'Рэй',
                'lastname' => 'Брэдбери',
                'middlename' => 'Дуглас',
                'dob' => 1920,
                'dod' => 2012
            ),
        ));
        \DB::table('authors')->insert(array(
            array(
                'firstname' => 'Грегори',
                'lastname' => 'Робертс',
                'middlename' => 'Дэвид',
                'dob' => 1952,
                'dod' => null
            ),
        ));
        \DB::table('authors')->insert(array(
            array(
                'firstname' => 'Джордж',
                'lastname' => 'Оруэлл',
                'middlename' => '',
                'dob' => 1903,
                'dod' => 1950
            ),
        ));
        \DB::table('authors')->insert(array(
            array(
                'firstname' => 'Айн',
                'lastname' => 'Рэнд',
                'middlename' => '',
                'dob' => 1905,
                'dod' => 1982
            ),
        ));
        \DB::table('authors')->insert(array(
            array(
                'firstname' => 'Аркадий',
                'lastname' => 'Стругацкий',
                'middlename' => 'Натанович',
                'dob' => 1925,
                'dod' => 1991
            ),
        ));
        \DB::table('authors')->insert(array(
            array(
                'firstname' => 'Борис',
                'lastname' => 'Стругацкий',
                'middlename' => 'Натанович',
                'dob' => 1933,
                'dod' => 2012
            ),
        ));
    }
}
