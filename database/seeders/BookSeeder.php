<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('books')->insert(array(
            array(
                'name' => '451° по Фаренгейту',
                'description' => 'Мастер мирового масштаба, совмещающий в литературе несовместимое. Создатель таких ярчайших шедевров, как "Марсианские хроники", "451° по Фаренгейту", "Вино из одуванчиков" и так далее и так далее.',
                'release_date' => 1953
            ),
        ));
        \DB::table('books')->insert(array(
            array(
                'name' => 'Шантарам',
                'description' => 'Эта преломленная в художественной форме исповедь человека, который сумел выбраться из бездны и уцелеть, разошлась по миру тиражом четыре миллиона экземпляров (из них полмиллиона – в России) и заслужила восторженные сравнения с произведениями лучших писателей Нового времени, от Мелвилла до Хемингуэя. Подобно автору, герой этого романа много лет скрывался от закона.',
                'release_date' => 2003
            ),
        ));
        \DB::table('books')->insert(array(
            array(
                'name' => '1984',
                'description' => 'Своеобразный антипод второй великой антиутопии XX века - "О дивный новый мир" Олдоса Хаксли.',
                'release_date' => 1949
            ),
        ));
        \DB::table('books')->insert(array(
            array(
                'name' => 'Атлант расправил плечи',
                'description' => 'Основная идея романа заключается в том, что мир держится на талантливых творцах-одиночках, которых автор сравнивает с мифическим титаном Атлантом, держащим на плечах небесный свод. Если эти люди перестанут творить («держать небо на плечах»), то мир рухнет. Именно это и происходит в романе, когда творцы-Атланты проигрывают в борьбе с правительством социалистического толка.',
                'release_date' => 1957
            ),
        ));
        \DB::table('books')->insert(array(
            array(
                'name' => 'Тень горы',
                'description' => 'Теперь Лину предстоит выполнить последнее поручение, данное ему Кадербхаем, завоевать доверие живущего на горе мудреца, сберечь голову в неудержимо разгорающемся конфликте новых главарей мафии, но главное – обрести любовь и веру.',
                'release_date' => 2016
            ),
        ));
        \DB::table('books')->insert(array(
            array(
                'name' => 'Понедельник начинается в субботу',
                'description' => 'Фантастическая юмористическая повесть братьев Стругацких, одно из наиболее своеобразных воплощений советской утопии 1960-х годов, художественная реализация мечты авторов о возможности для современного талантливого человека сосредоточиться на научном творчестве и познании тайн Вселенной.',
                'release_date' => 1964
            ),
        ));
    }
}
